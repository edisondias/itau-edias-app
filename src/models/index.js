let users = {
  1: {
    id: '1',
    username: 'Robin Wieruch',
  },
  2: {
    id: '2',
    username: 'Dave Davids',
  },
  3: {
    id: '3',
    username: 'Edison Dias',
  },
  4: {
    id: '4',
    username: 'Novo Usuario',
  },
};

let messages = {
  1: {
    id: '1',
    text: 'Hello World',
    userId: '1',
  },
  2: {
    id: '2',
    text: 'Bye World',
    userId: '2',
  },
  3: {
    id: '3',
    text: 'Hold on World',
    userId: '3',
  },
  4: {
    id: '4',
    text: 'Nova Mensagem',
    userId: '4',
  },
};

export default {
  users,
  messages,
};
